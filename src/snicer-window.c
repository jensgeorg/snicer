/* snicer-window.c
 *
 * Copyright 2018 Jens Georg
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "snicer-config.h"
#include "snicer-window.h"
#include "snicer-thumbnail.h"
#include "snicer-document.h"

#include <glib/gi18n.h>
#include <cairo.h>

struct _SnicerWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  GtkHeaderBar        *header_bar;
  GtkLabel            *label;
  GtkButton           *main_button_recent;
  GtkWidget           *flowbox;
  GtkWidget           *scrolled;
  GtkWidget           *stack;
  GtkWidget           *window_menu;

  /* Other members */
  SnicerDocument      *document;
  GCancellable        *cancellable;
};


static gboolean on_window_about_to_delete (SnicerWindow *self, GdkEvent *event, gpointer data);
static void on_open (GSimpleAction *action, GVariant *parameter, gpointer data);
static void on_about (GSimpleAction *action, GVariant *target, gpointer user_data);
static void on_save (GSimpleAction *action, GVariant *parameter, gpointer data);
static void on_rotate (GSimpleAction *action, GVariant *parameter, gpointer data);
static void on_remove (GSimpleAction *action, GVariant *parameter, gpointer data);

/*
 * Helpers
 */
static guint*
collect_selected_pages (GtkWidget *flowbox, guint *size)
{
    g_autoptr(GList) children = gtk_flow_box_get_selected_children (GTK_FLOW_BOX (flowbox));
    GList *it = NULL;
    *size = g_list_length (children);
    guint *pages = g_new0 (guint, *size);
    guint *pages_iter = NULL;

    for (it = children, pages_iter = pages; it != NULL; it = it->next, pages_iter++) {
        g_object_get (G_OBJECT (gtk_bin_get_child (GTK_BIN (it->data))),
                      "page-nr", pages_iter, NULL);
        // Page-Nr starts at 1, while all other indexing starts at 0...
        *pages_iter -= 1;
    }

    return pages;
}

static gboolean do_confirm_action_and_save (SnicerWindow *self,
                                            const char *title,
                                            const char *subtitle,
                                            const char *affirmative_button_text)
{
  // No document or not modified - nothing to do
  if (self->document == NULL || !snicer_document_is_modified (self->document))
    return TRUE;

  GtkWidget *dialog = gtk_message_dialog_new (GTK_WINDOW (self),
      GTK_DIALOG_MODAL | GTK_DIALOG_USE_HEADER_BAR,
      GTK_MESSAGE_QUESTION,
      GTK_BUTTONS_NONE,
      "%s", title);
  gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog), "%s", subtitle);

  GtkWidget *exit = gtk_dialog_add_button (GTK_DIALOG (dialog), affirmative_button_text, GTK_RESPONSE_NO);
  gtk_dialog_add_button (GTK_DIALOG (dialog), _("_Cancel"), GTK_RESPONSE_CANCEL);
  gtk_dialog_add_button (GTK_DIALOG (dialog), _("_Save"), GTK_RESPONSE_OK);
  gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
  gtk_style_context_add_class (gtk_widget_get_style_context (exit),
      GTK_STYLE_CLASS_DESTRUCTIVE_ACTION);

  gint result = gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);

  if (result == GTK_RESPONSE_CANCEL || result == GTK_RESPONSE_CLOSE) {
    return FALSE;
  }

  if (result == GTK_RESPONSE_OK) {
    on_save (NULL, NULL, self);
  }

  return TRUE;
}

static void
snicer_window_update_title (SnicerWindow *self)
{
    g_autofree char *title = g_strdup_printf ("%s %s",
            snicer_document_get_file_name (self->document),
            snicer_document_is_modified (self->document) ? "•" : "");
    gtk_header_bar_set_title (self->header_bar, title);
}

static void on_save_as (GSimpleAction *action, GVariant *parameter, gpointer data)
{
  SnicerWindow *self = SNICER_WINDOW (data);
  GtkWidget *file_chooser = gtk_file_chooser_dialog_new (_("Save File"),
                                              GTK_WINDOW (self),
                                              GTK_FILE_CHOOSER_ACTION_SAVE,
                                              _("_Cancel"),
                                              GTK_RESPONSE_CANCEL,
                                              _("_Save"),
                                              GTK_RESPONSE_ACCEPT,
                                              NULL);

  gtk_file_chooser_set_do_overwrite_confirmation (GTK_FILE_CHOOSER (file_chooser), TRUE);
  gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER (file_chooser),
                                     snicer_document_get_file_name (self->document));
  gtk_file_chooser_set_local_only (GTK_FILE_CHOOSER (file_chooser), FALSE);

  int response = gtk_dialog_run (GTK_DIALOG (file_chooser));
  if (response == GTK_RESPONSE_ACCEPT) {
    g_autoptr(GFile) file = gtk_file_chooser_get_file (GTK_FILE_CHOOSER (file_chooser));
    GError *error = NULL;

    if (!snicer_document_write (self->document, file, &error)) {
      g_autofree char *uri = g_file_get_uri (file);
      GtkWidget *message_box = gtk_message_dialog_new (GTK_WINDOW (self),
              GTK_DIALOG_MODAL | GTK_DIALOG_USE_HEADER_BAR,
              GTK_MESSAGE_ERROR,
              GTK_BUTTONS_OK,
              _("Failed to save the file to %s : %s"), uri,
              error->message);

      gtk_dialog_run (GTK_DIALOG (message_box));
      gtk_widget_destroy (message_box);
      g_error_free (error);
    }
  }
  gtk_widget_destroy (file_chooser);
}

static GActionEntry actions[] = {
  {"open", on_open },
  {"save", on_save },
  {"save-as", on_save_as },
  {"rotate", on_rotate, "i" },
  {"remove", on_remove },
  {"about", on_about }
};

G_DEFINE_TYPE (SnicerWindow, snicer_window, GTK_TYPE_APPLICATION_WINDOW)

static void snicer_window_recent_item_activated (SnicerWindow *self, GtkRecentChooser *data);

static void on_open (GSimpleAction *action, GVariant *parameter, gpointer data)
{
  SnicerWindow *self = SNICER_WINDOW (data);
  GtkWidget *file_chooser = NULL;
  GtkFileFilter *filter = NULL;

  file_chooser = gtk_file_chooser_dialog_new (_("Open File"),
                                              GTK_WINDOW (self),
                                              GTK_FILE_CHOOSER_ACTION_OPEN,
                                              _("_Cancel"),
                                              GTK_RESPONSE_CANCEL,
                                              _("_Open"),
                                              GTK_RESPONSE_ACCEPT,
                                              NULL);
  gtk_file_chooser_set_local_only (GTK_FILE_CHOOSER (file_chooser), FALSE);
  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name (filter, _("PDF files"));
  gtk_file_filter_add_pattern (filter, "*.pdf");
  gtk_file_filter_add_mime_type (filter, "application/pdf");
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (file_chooser), filter);

  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name (filter, _("All files"));
  gtk_file_filter_add_pattern (filter, "*");
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (file_chooser), filter);

  int response = gtk_dialog_run (GTK_DIALOG (file_chooser));
  g_autofree char *file = gtk_file_chooser_get_uri (GTK_FILE_CHOOSER (file_chooser));
  gtk_widget_hide (file_chooser);

  if (response == GTK_RESPONSE_ACCEPT) {
    snicer_window_open_document (self, file);
  }
  gtk_widget_destroy (file_chooser);
}

static void
on_remove (GSimpleAction *action, GVariant *parameter, gpointer data)
{
    SnicerWindow *self = SNICER_WINDOW (data);

    guint n_pages;
    g_autofree guint *pages = collect_selected_pages (self->flowbox, &n_pages);

    GError *error = NULL;
    if (!snicer_document_remove_pages (self->document, pages, n_pages, &error)) {
        GtkWidget *message_box = gtk_message_dialog_new (GTK_WINDOW (self),
                                                         GTK_DIALOG_MODAL |
                                                         GTK_DIALOG_USE_HEADER_BAR,
                                                         GTK_MESSAGE_ERROR,
                                                         GTK_BUTTONS_OK,
                                                         _("Failed to remove pages from the document: %s"),
                                                         error->message);

        gtk_dialog_run (GTK_DIALOG (message_box));
        gtk_widget_destroy (message_box);
        g_error_free (error);
    }
}

static void on_rotate (GSimpleAction *action, GVariant *parameter, gpointer data) {
    int direction = g_variant_get_int32 (parameter);
    SnicerWindow *self = SNICER_WINDOW (data);

    guint n_pages;
    g_autofree guint *pages = collect_selected_pages (self->flowbox, &n_pages);

    GError *err = NULL;
    if (!snicer_document_rotate_pages (self->document, pages, n_pages, direction, &err)) {
        GtkWidget *message_box = gtk_message_dialog_new (GTK_WINDOW (self),
                GTK_DIALOG_MODAL | GTK_DIALOG_USE_HEADER_BAR,
                GTK_MESSAGE_ERROR,
                GTK_BUTTONS_OK,
                _("Failed to rotate the pages : %s"),
                err->message);
        gtk_dialog_run (GTK_DIALOG (message_box));
        gtk_widget_destroy (message_box);
        g_error_free (err);
    }
}

static void
on_save (GSimpleAction *action, GVariant *parameter, gpointer data)
{
  SnicerWindow *self = SNICER_WINDOW (data);
  GError *error = NULL;

  if (!snicer_document_write (self->document, NULL, &error)) {
      GtkWidget *message_box = gtk_message_dialog_new (GTK_WINDOW (self),
              GTK_DIALOG_MODAL | GTK_DIALOG_USE_HEADER_BAR,
              GTK_MESSAGE_ERROR,
              GTK_BUTTONS_OK,
              _("Failed to save the file: %s"),
              error->message);

      gtk_dialog_run (GTK_DIALOG (message_box));
      gtk_widget_destroy (message_box);
      g_error_free (error);
  }
}

static void
on_selected_children_changed (SnicerWindow *self, GtkFlowBox *flowbox)
{
  static const char *selection_actions[] = { "rotate", "remove" };

  GList *children = gtk_flow_box_get_selected_children (flowbox);
  for (int i = 0; i < G_N_ELEMENTS (selection_actions); i++) {
    GAction *action = g_action_map_lookup_action (G_ACTION_MAP (self), selection_actions[i]);
    g_simple_action_set_enabled (G_SIMPLE_ACTION (action), children != NULL);
  }

  if (children != NULL) {
    g_list_free (children);
  }
}

static void
snicer_window_class_init (SnicerWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Snicer/snicer-window.ui");
  gtk_widget_class_bind_template_child (widget_class, SnicerWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, SnicerWindow, main_button_recent);
  gtk_widget_class_bind_template_child (widget_class, SnicerWindow, flowbox);
  gtk_widget_class_bind_template_child (widget_class, SnicerWindow, scrolled);
  gtk_widget_class_bind_template_child (widget_class, SnicerWindow, stack);
  gtk_widget_class_bind_template_child (widget_class, SnicerWindow, window_menu);
}

static void
snicer_window_init (SnicerWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  gtk_widget_set_name (self->scrolled, "document-view");

  gtk_flow_box_set_vadjustment (GTK_FLOW_BOX (self->flowbox),
                                gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (self->scrolled)));
  gtk_flow_box_set_hadjustment (GTK_FLOW_BOX (self->flowbox),
                                gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW (self->scrolled)));

  g_signal_connect (G_OBJECT (self), "delete-event", G_CALLBACK (on_window_about_to_delete), NULL);

  g_autoptr(GtkBuilder) builder = gtk_builder_new_from_resource ("/org/gnome/Snicer/window-menu.ui");
  GObject *object = gtk_builder_get_object (builder, "snicer-window-menu");
  gtk_menu_button_set_menu_model (GTK_MENU_BUTTON (self->window_menu), G_MENU_MODEL (object));

  GtkWidget *recent = gtk_recent_chooser_menu_new ();
  GtkRecentFilter *filter = gtk_recent_filter_new ();
  gtk_recent_filter_add_mime_type (filter, "application/pdf");
  gtk_recent_chooser_add_filter (GTK_RECENT_CHOOSER (recent), filter);
  gtk_recent_chooser_set_show_not_found (GTK_RECENT_CHOOSER (recent), FALSE);
  gtk_recent_chooser_set_show_icons (GTK_RECENT_CHOOSER (recent), FALSE);
  gtk_recent_chooser_set_local_only (GTK_RECENT_CHOOSER (recent), FALSE);
  gtk_recent_chooser_set_sort_type (GTK_RECENT_CHOOSER (recent), GTK_RECENT_SORT_MRU);
  g_signal_connect_swapped (self->flowbox, "selected-children-changed", G_CALLBACK (on_selected_children_changed), self);

  g_action_map_add_action_entries (G_ACTION_MAP (self), actions, G_N_ELEMENTS (actions), self);
  on_selected_children_changed (self, GTK_FLOW_BOX (self->flowbox));
  g_signal_connect_swapped (recent, "item-activated", G_CALLBACK(snicer_window_recent_item_activated), self);
  gtk_menu_button_set_popup (GTK_MENU_BUTTON (self->main_button_recent), recent);

  GAction *action = g_action_map_lookup_action (G_ACTION_MAP (self), "save");
  g_simple_action_set_enabled (G_SIMPLE_ACTION (action), FALSE);
  action = g_action_map_lookup_action (G_ACTION_MAP (self), "save-as");
  g_simple_action_set_enabled (G_SIMPLE_ACTION (action), FALSE);

  self->cancellable = g_cancellable_new ();
}

static GtkWidget *
on_flow_box_create_item (gpointer item, gpointer user_data)
{
    return GTK_WIDGET (snicer_thumbnail_new (SNICER_PAGE (item)));
}
void
snicer_window_open_document (SnicerWindow *self, const char *uri)
{
  if (!do_confirm_action_and_save (self,
        _("Close current document without saving?"),
        _("Opening another document without saving the currently open document will cause all your changes to be lost"),
        _("Continue without saving"))) {
    return;
  }

  // Stop pending operations
  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);
  self->cancellable = g_cancellable_new ();

  g_clear_object (&self->document);
  self->document = snicer_document_new ();
  g_autoptr(GFile) file = g_file_new_for_uri (uri);

  GError *error = NULL;
  if (!snicer_document_open (self->document, file, NULL, &error)) {
    GtkWidget *message_box = gtk_message_dialog_new (GTK_WINDOW (self),
                            GTK_DIALOG_MODAL | GTK_DIALOG_USE_HEADER_BAR,
                            GTK_MESSAGE_ERROR,
                            GTK_BUTTONS_OK,
                            _("Failed to open %s : %s"),
                            uri,
                            error->message);
    gtk_dialog_run (GTK_DIALOG (message_box));
    gtk_widget_destroy (message_box);
    g_error_free (error);
  } else {
    GtkRecentManager *mgr = gtk_recent_manager_get_default ();
    gtk_recent_manager_add_item (mgr, uri);
    snicer_window_update_title (self);
    //snicer_window_update_thumbnails (self);
    gtk_flow_box_bind_model (GTK_FLOW_BOX (self->flowbox),
                             G_LIST_MODEL (self->document),
                             on_flow_box_create_item, NULL, NULL);
    g_signal_connect_swapped (G_OBJECT (self->document), "notify::modified", G_CALLBACK (snicer_window_update_title), self);

    GAction *action = g_action_map_lookup_action (G_ACTION_MAP (self), "save");
    g_simple_action_set_enabled (G_SIMPLE_ACTION (action), TRUE);

    action = g_action_map_lookup_action (G_ACTION_MAP (self), "save-as");
    g_simple_action_set_enabled (G_SIMPLE_ACTION (action), TRUE);
    gtk_stack_set_visible_child_name (GTK_STACK (self->stack), "document-view");
  }
}

static void
snicer_window_recent_item_activated (SnicerWindow *self, GtkRecentChooser *data)
{
  g_autofree char *uri = gtk_recent_chooser_get_current_uri (data);

  snicer_window_open_document (self, uri);
}

static gboolean
on_window_about_to_delete (SnicerWindow *self, GdkEvent *event, gpointer data)
{
  gboolean result = do_confirm_action_and_save (self,
      _("Leave application without saving?"),
      _("Leaving the application without saving the document will cause all your changes to be lost"),
      _("Close without saving"));

  if (result) {
    g_cancellable_cancel (self->cancellable);
  }

  return !result;
}

static void
on_about (GSimpleAction *action, GVariant *target, gpointer user_data)
{
  g_autoptr(GtkBuilder) builder = gtk_builder_new_from_resource ("/org/gnome/Snicer/about.ui");
  GtkWidget *about = GTK_WIDGET (gtk_builder_get_object (builder, "snicer-about"));
  gtk_window_set_transient_for (GTK_WINDOW (about), GTK_WINDOW (user_data));

  gtk_dialog_run (GTK_DIALOG (about));
  gtk_widget_destroy (about);
}


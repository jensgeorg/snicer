#include "podofo-page.h"
#include "podofo-error.h"

#include <podofo/podofo.h>

namespace pdf = PoDoFo;

struct _PodofoPagePrivate {
    pdf::PdfPage *page;
};
typedef struct _PodofoPagePrivate PodofoPagePrivate;

G_DEFINE_TYPE_WITH_PRIVATE (PodofoPage, podofo_page, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_PAGE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static gboolean
podofo_page_rotate_internal (PodofoPage *self,
                             PodofoPageRotationDirection direction,
                             GError **error);

static void
podofo_page_finalize (GObject *object)
{
  //PodofoPage *self = PODOFO_PAGE (object);

  G_OBJECT_CLASS (podofo_page_parent_class)->finalize (object);
}

static void
podofo_page_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  PodofoPage *self = PODOFO_PAGE (object);
  auto priv = reinterpret_cast<PodofoPagePrivate *>(podofo_page_get_instance_private (self));

  switch (prop_id)
    {
    case PROP_PAGE:
      g_value_set_pointer (value, reinterpret_cast<gpointer>(priv->page));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
podofo_page_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  PodofoPage *self = PODOFO_PAGE (object);
  auto priv = reinterpret_cast<PodofoPagePrivate *>(podofo_page_get_instance_private (self));

  switch (prop_id)
    {
    case PROP_PAGE:
      priv->page = reinterpret_cast<pdf::PdfPage *>(g_value_get_pointer (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
podofo_page_class_init (PodofoPageClass *klass)
{
  klass->rotate = podofo_page_rotate_internal;

  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = podofo_page_finalize;
  object_class->get_property = podofo_page_get_property;
  object_class->set_property = podofo_page_set_property;

  properties[PROP_PAGE] = g_param_spec_pointer ("page",
          "page",
          "page",
          static_cast<GParamFlags>
          (G_PARAM_READWRITE |
           G_PARAM_CONSTRUCT_ONLY |
           G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class, PROP_PAGE, properties[PROP_PAGE]);
}

static void
podofo_page_init (PodofoPage *self)
{
}

static gboolean
podofo_page_rotate_internal (PodofoPage *self,
                             PodofoPageRotationDirection direction,
                             GError **error)
{
    g_return_val_if_fail (PODOFO_IS_PAGE (self), FALSE);
    auto priv = reinterpret_cast<PodofoPagePrivate *>(podofo_page_get_instance_private (self));

    try {
        auto rotation_deg = (priv->page->GetRotationRaw () + (direction * 90) + 360) % 360;
        priv->page->SetRotationRaw (rotation_deg);
    } catch (pdf::PdfError &err) {
        error << err;

        return FALSE;
    }

    return TRUE;
}

gboolean
podofo_page_rotate (PodofoPage *self,
                    PodofoPageRotationDirection direction,
                    GError **error)
{
    PodofoPageClass *klass = PODOFO_PAGE_GET_CLASS (self);

    g_return_val_if_fail (klass->rotate != nullptr, FALSE);

    return klass->rotate (self, direction, error);
}

guint
podofo_page_get_page_number (PodofoPage *self)
{
    g_return_val_if_fail (PODOFO_IS_PAGE (self), 0);
    auto priv = reinterpret_cast<PodofoPagePrivate *>(podofo_page_get_instance_private (self));

    return priv->page->GetPageNumber ();
}

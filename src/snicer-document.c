#include "snicer-document.h"
#include "snicer-page.h"

#include <gio/gio.h>
#include <glib/gi18n.h>

#include <poppler.h>

struct _SnicerDocument
{
  PodofoDocument parent_instance;

  PopplerDocument *poppler_doc;
  GFile *file;
  GFileInfo *file_info;
  gboolean modified;
  GMutex thumb_cache_lock;
  GPtrArray *pages;
};

static void
snicer_document_list_model_init (gpointer g_iface, gpointer iface_data);

G_DEFINE_TYPE_WITH_CODE (SnicerDocument, snicer_document, PODOFO_TYPE_DOCUMENT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, snicer_document_list_model_init))

enum {
  PROP_0,
  PROP_MODIFIED,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
snicer_document_set_modified (SnicerDocument *self, gboolean modified);

static PodofoPage *
snicer_document_internal_get_page (PodofoDocument *self,
                                   guint            page,
                                   gpointer         page_ptr);

static gboolean
snicer_document_internal_delete_page (PodofoDocument *doc,
                                      guint           page_nr,
                                      GError        **error);

SnicerDocument *
snicer_document_new (void)
{
  return SNICER_DOCUMENT (g_object_new (SNICER_TYPE_DOCUMENT, NULL));
}

static void
snicer_document_finalize (GObject *object)
{
  //SnicerDocument *self = (SnicerDocument *)object;

  G_OBJECT_CLASS (snicer_document_parent_class)->finalize (object);
}

static void
snicer_document_dispose (GObject *object)
{
  SnicerDocument *self = (SnicerDocument *)object;

  snicer_document_close (self, NULL);

  G_OBJECT_CLASS (snicer_document_parent_class)->finalize (object);
}

static void
snicer_document_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  SnicerDocument *self = SNICER_DOCUMENT (object);

  switch (prop_id)
    {
    case PROP_MODIFIED:
      g_value_set_boolean (value, self->modified);
    break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
snicer_document_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
//  SnicerDocument *self = SNICER_DOCUMENT (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
snicer_document_class_init (SnicerDocumentClass *klass)
{
  PodofoDocumentClass *document_class = PODOFO_DOCUMENT_CLASS (klass);

  document_class->get_page = snicer_document_internal_get_page;
  document_class->delete_page = snicer_document_internal_delete_page;

  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = snicer_document_finalize;
  object_class->dispose = snicer_document_dispose;
  object_class->get_property = snicer_document_get_property;
  object_class->set_property = snicer_document_set_property;

  properties[PROP_MODIFIED] = g_param_spec_boolean ("modified",
          "modified",
          "modified",
          FALSE,
          (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class, PROP_MODIFIED, properties[PROP_MODIFIED]);
}

static void
snicer_document_init (SnicerDocument *self)
{
    g_mutex_init (&self->thumb_cache_lock);
}

static void
snicer_page_unref (SnicerPage *page)
{
    if (page == NULL)
        return;

    g_object_unref (G_OBJECT (page));
}

gboolean
snicer_document_open (SnicerDocument *self, GFile *file, GCancellable *cancellable, GError **error)
{
    snicer_document_close (self, NULL);

    if (!podofo_document_open (PODOFO_DOCUMENT(self), file, error))
        return FALSE;

    g_autoptr(GFileInfo) info = g_file_query_info (file, "standard::*",G_FILE_QUERY_INFO_NONE, cancellable, error);
    if (info == NULL)
        return FALSE;

    self->poppler_doc = poppler_document_new_from_gfile (file, NULL, cancellable, error);
    if (self->poppler_doc == NULL)
        return FALSE;

    self->pages = g_ptr_array_new_full (podofo_document_get_n_pages (PODOFO_DOCUMENT (self)),
                                        (GDestroyNotify) snicer_page_unref);
    g_ptr_array_set_size (self->pages, podofo_document_get_n_pages (PODOFO_DOCUMENT (self)));

    self->file = G_FILE (g_object_ref (file));
    self->file_info = g_steal_pointer (&info);

    snicer_document_set_modified (self, FALSE);

    return TRUE;
}

gboolean
snicer_document_close (SnicerDocument *self, GError **error)
{
    g_clear_object (&self->poppler_doc);
    g_clear_object (&self->file);
    g_clear_object (&self->file_info);

    return FALSE;
}

gboolean
snicer_document_write (SnicerDocument *self, GFile *file, GError **error)
{
    gboolean retval = podofo_document_write (PODOFO_DOCUMENT (self), file, error);
    
    if (!retval)
        return FALSE;

    if (!g_file_equal (file, self->file))
        return snicer_document_open(self, file, NULL, error);

    return TRUE;
}

gboolean
snicer_document_rotate_pages (SnicerDocument *self, guint *pages, gsize pages_length, gint direction, GError **error)
{
    if (pages_length == 0)
        return TRUE;

    for (gsize i = 0; i < pages_length; i++) {
        guint current_page = pages[i];
        g_debug ("Rotating page %u", current_page);
        PodofoPage *page = podofo_document_get_page (PODOFO_DOCUMENT (self), current_page);
        if (!podofo_page_rotate (page, direction, error))
            g_critical ("Failed to rotate page %u", current_page);
    }

    return TRUE;
}

typedef struct {
    GListModel *list;
    guint position;
    guint removed;
    guint added;
} SnicerDocumentModificationNotify;

gboolean
snicer_docoment_modify_notify (gpointer user_data)
{

    GList *list = (GList *) user_data;

    while (list != NULL) {
        SnicerDocumentModificationNotify *notify = (SnicerDocumentModificationNotify *)list->data;
        g_debug ("Would emit removing %u starting at %u", notify->removed, notify->position);
        g_list_model_items_changed (notify->list, notify->position, notify->removed, notify->added);
        g_slice_free (SnicerDocumentModificationNotify, list->data);
        list = g_list_delete_link (list, list);
    }

    return FALSE;
}

gboolean
snicer_document_remove_pages (SnicerDocument  *self,
                              guint           *pages,
                              gsize            pages_length,
                              GError         **error)
{
    snicer_document_set_modified (self, TRUE);
    GList *notification_data = NULL;
    gint position = 0;
    gint count = 0;


    for (gsize i = 0; i < pages_length; i++) {
        g_debug ("Removing page %u", pages[i]);
        if (pages[i] != position + count) {
            if (count > 0) {
                SnicerDocumentModificationNotify *notify = g_slice_new0 (SnicerDocumentModificationNotify);
                notify->list = G_LIST_MODEL (self);
                notify->position = position;
                notify->removed = count;
                notification_data = g_list_prepend (notification_data, notify);
            }

            position = pages[i];
            count = 1;
        } else {
            count++;
        }

        // As we remove pages one by one, we need to decrease the page number
        // by the number of pages that were already removed
        if (!podofo_document_delete_page (PODOFO_DOCUMENT (self), pages[i] - i, error))
            return FALSE;
    }

    SnicerDocumentModificationNotify *notify = g_slice_new0 (SnicerDocumentModificationNotify);
    notify->list = G_LIST_MODEL (self);
    notify->position = position;
    notify->removed = count;
    notification_data = g_list_prepend (notification_data, notify);

    g_idle_add (snicer_docoment_modify_notify, notification_data);

    return TRUE;
}

gboolean
snicer_document_move_pages (SnicerDocument  *self,
                            guint           *pages,
                            gsize            pages_length,
                            guint            before_page,
                            GError         **error)
{
    return FALSE;
}

gchar *
snicer_document_get_page_title (SnicerDocument *self, guint page)
{
    return g_strdup_printf(_("Page %d of %d"), page + 1,
                           podofo_document_get_n_pages (PODOFO_DOCUMENT (self)));
}

const gchar *
snicer_document_get_file_name (SnicerDocument *self)
{
    return g_file_info_get_display_name(self->file_info);
}

GQuark snicer_document_error_quark (void)
{
    return g_quark_from_static_string ("spicer-document-error-quark");
}

gboolean snicer_document_is_modified (SnicerDocument *self)
{
    return self->modified;
}

static void
snicer_document_set_modified (SnicerDocument *self, gboolean modified)
{
    if (modified != self->modified)
        self->modified = modified;

    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_MODIFIED]);
}

/////////////////////////////////////////////
// PodofoDocument implementation
/////////////////////////////////////////////

static PodofoPage *
snicer_document_internal_get_page (PodofoDocument *doc,
                                   guint            page_nr,
                                   gpointer         page_ptr)
{
    g_return_val_if_fail (SNICER_IS_DOCUMENT (doc), NULL);

    SnicerDocument *self = SNICER_DOCUMENT (doc);
    SnicerPage *page = NULL;

    if (page_nr < self->pages->len)
        page = g_ptr_array_index (self->pages, page_nr);

    if (page == NULL) {
        PopplerPage *poppler_page = poppler_document_get_page (self->poppler_doc, page_nr);
        page = g_object_new (SNICER_TYPE_PAGE,
                             "page", page_ptr,
                             "poppler-page", poppler_page,
                             NULL);
        g_ptr_array_insert (self->pages, page_nr, page);
    }

    return PODOFO_PAGE (g_object_ref (page));
}

static gboolean
snicer_document_internal_delete_page (PodofoDocument *doc,
                                      guint           page_nr,
                                      GError        **error)
{
    SnicerDocument *self = SNICER_DOCUMENT (doc);

    g_ptr_array_remove_index (self->pages, page_nr);

    return PODOFO_DOCUMENT_CLASS (snicer_document_parent_class)->delete_page (doc, page_nr, error);
}

/////////////////////////////////////////////
// GListModel implementation
/////////////////////////////////////////////

static GType
snicer_document_list_model_get_item_type (GListModel *list)
{
  return SNICER_TYPE_PAGE;
}

static guint
snicer_document_list_model_get_n_items (GListModel *list)
{
  return podofo_document_get_n_pages (PODOFO_DOCUMENT (list));
}

static gpointer
snicer_document_list_model_get_item (GListModel *list, guint index)
{
    return podofo_document_get_page (PODOFO_DOCUMENT (list), index);
}

static void
snicer_document_list_model_init (gpointer g_iface, gpointer iface_data)
{
  GListModelInterface *iface = (GListModelInterface *) (g_iface);
  iface->get_item_type = snicer_document_list_model_get_item_type;
  iface->get_n_items = snicer_document_list_model_get_n_items;
  iface->get_item = snicer_document_list_model_get_item;
}

#pragma once

#include "snicer-page.h"

#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define SNICER_TYPE_THUMBNAIL (snicer_thumbnail_get_type())

G_DECLARE_FINAL_TYPE (SnicerThumbnail, snicer_thumbnail, SNICER, THUMBNAIL, GtkBox)

SnicerThumbnail *snicer_thumbnail_new (SnicerPage *page);

G_END_DECLS

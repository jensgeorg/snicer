#include <glib.h>

#include <podofo/podofo.h>

#include <codecvt>
#include <locale>
#include <string>

namespace pdf = PoDoFo;

namespace snicer {

class LogMessageCallback  {
    std::map<pdf::PdfLogSeverity, GLogLevelFlags> map {
        {pdf::PdfLogSeverity::Error, G_LOG_LEVEL_CRITICAL},
        {pdf::PdfLogSeverity::Warning, G_LOG_LEVEL_WARNING},
        {pdf::PdfLogSeverity::Information, G_LOG_LEVEL_INFO},
        {pdf::PdfLogSeverity::Debug, G_LOG_LEVEL_DEBUG},
        {pdf::PdfLogSeverity::None, G_LOG_LEVEL_MESSAGE}
    };

public:
    LogMessageCallback() {
        pdf::PdfCommon::SetLogMessageCallback ([&] (auto severity, auto &message) {
            g_autofree char *msg = g_strndup (message.data (), message.size ());
            for (int len = strlen (msg); len > 0; len--) {
                if (g_ascii_iscntrl (msg[len])) {
                    msg[len] = '\0';
                }
            }

            g_log ("PoFoFo", map[severity], "%s", msg);
        });
        pdf::PdfCommon::SetMaxLoggingSeverity (pdf::PdfLogSeverity::Debug);
    }
};

static LogMessageCallback log_message_callback_enabler;

};

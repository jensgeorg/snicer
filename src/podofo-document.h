#pragma once

#include "podofo-page.h"

#include <glib-object.h>
#include <gio/gio.h>

G_BEGIN_DECLS

#define PODOFO_TYPE_DOCUMENT (podofo_document_get_type())

G_DECLARE_DERIVABLE_TYPE (PodofoDocument, podofo_document, PODOFO, DOCUMENT, GObject)

struct _PodofoDocumentClass {
  GObjectClass parent_class;

  PodofoPage *(* get_page)(PodofoDocument *self, guint page, gpointer page_ptr);
  gboolean (* delete_page)(PodofoDocument *self, guint page, GError **error);

  gpointer padding[12];
};

PodofoDocument *
podofo_document_new   (void);

gboolean
podofo_document_open  (PodofoDocument *self,
                       GFile          *file,
                       GError        **error);

gboolean
podofo_document_write (PodofoDocument *self,
                       GFile          *file,
                       GError        **error);
void
podofo_document_close (PodofoDocument *self);

gboolean
podofo_document_rotate_page (PodofoDocument *self,
                             guint           page,
                             gint            direction,
                             GError         **error);

guint
podofo_document_get_n_pages (PodofoDocument *self);

gboolean
podofo_document_delete_page (PodofoDocument *self,
                             guint           page,
                             GError         **error);

PodofoPage *
podofo_document_get_page    (PodofoDocument *self,
                             guint           page);

G_END_DECLS

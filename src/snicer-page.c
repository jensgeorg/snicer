#include "snicer-page.h"

#include <cairo-gobject.h>
#include <gtk/gtk.h>
#include <poppler.h>
#include <glib/gi18n.h>
#include <gio/gio.h>

// We need this to protect against concurrent poppler thumbnail rendering
static GMutex global_poppler_lock;

struct _SnicerPage
{
        PodofoPage parent_instance;
        PopplerPage *page;
        char *title;
        cairo_surface_t *thumbnail;
        guint page_nr;
        GCancellable *cancellable;

        // Mutex to protect the poppler page while background thumbnailing
        GMutex page_lock;
};

G_DEFINE_TYPE (SnicerPage, snicer_page, PODOFO_TYPE_PAGE)

enum {
    PROP_0,
    PROP_TITLE,
    PROP_POPPLER_PAGE,
    PROP_THUMBNAIL,
    N_PROPS
};

static GParamSpec *properties [N_PROPS];

static gboolean
snicer_page_rotate_internal (PodofoPage                      *page,
                             PodofoPageRotationDirection   direction,
                             GError                         **error);

static void
snicer_page_finalize (GObject *object)
{
        SnicerPage *self = (SnicerPage *)object;
        g_clear_pointer (&self->thumbnail, cairo_surface_destroy);
        g_clear_pointer (&self->title, g_free);
        g_clear_object (&self->page);

        G_OBJECT_CLASS (snicer_page_parent_class)->finalize (object);
}

static void
snicer_page_dispose (GObject *object)
{
    SnicerPage *self = SNICER_PAGE (object);

    // Stop a possible background thumbnailing thread
    g_clear_pointer (&self->cancellable, g_cancellable_cancel);

    G_OBJECT_CLASS (snicer_page_parent_class)->dispose (object);
}

static void
snicer_page_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
        SnicerPage *self = SNICER_PAGE (object);

        switch (prop_id)
          {
          case PROP_TITLE:
              g_value_set_string (value, snicer_page_get_title (self));
              break;
          case PROP_THUMBNAIL:
              g_value_set_boxed (value, snicer_page_get_thumbnail (self));
              break;
          default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
          }
}

static void
snicer_page_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
        SnicerPage *self = SNICER_PAGE (object);

        switch (prop_id)
          {
          case PROP_TITLE:
              self->title = g_value_dup_string (value);
              break;
          case PROP_POPPLER_PAGE:
              self->page = g_value_get_object (value);
              break;
          default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
          }
}

static void
snicer_page_class_init (SnicerPageClass *klass)
{
    PodofoPageClass *page_class = PODOFO_PAGE_CLASS (klass);

    page_class->rotate = snicer_page_rotate_internal;

    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = snicer_page_finalize;
    object_class->dispose = snicer_page_dispose;

    object_class->get_property = snicer_page_get_property;
    object_class->set_property = snicer_page_set_property;

    properties[PROP_TITLE] = g_param_spec_string ("title", "title", "title",
                                                  NULL, G_PARAM_READABLE |
                                                  G_PARAM_STATIC_STRINGS);


    properties[PROP_POPPLER_PAGE] = g_param_spec_object ("poppler-page", "poppler-page", "poppler-page",
                                                 POPPLER_TYPE_PAGE,
                                                 G_PARAM_WRITABLE |
                                                 G_PARAM_CONSTRUCT_ONLY |
                                                 G_PARAM_STATIC_STRINGS);

    properties[PROP_THUMBNAIL] = g_param_spec_boxed("thumbnail", "thumbnail", "thumbnail",
                                                    CAIRO_GOBJECT_TYPE_SURFACE,
                                                      G_PARAM_READABLE |
                                                      G_PARAM_STATIC_STRINGS);

    g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
snicer_page_init (SnicerPage *self)
{
    g_mutex_init(&self->page_lock);
    self->cancellable = g_cancellable_new ();
}

static cairo_surface_t *
snicer_page_render_thumbnail (SnicerPage *self)
{
    g_autoptr (GMutexLocker) guard = g_mutex_locker_new (&self->page_lock);

    cairo_surface_t *thumbnail = NULL;

    double width, height;
    poppler_page_get_size (self->page, &width, &height);
    double scale = MIN (256.0 / width, 256.0 / height);

    thumbnail = cairo_image_surface_create (CAIRO_FORMAT_ARGB32,
                                            (int)(scale * width),
                                            (int)(scale * height));

    cairo_t *ctx = cairo_create (thumbnail);
    cairo_scale (ctx, scale, scale);
    cairo_set_source_rgb (ctx, 1.0, 1.0, 1.0);
    cairo_rectangle (ctx, 0.0, 0.0, width, height);
    cairo_fill (ctx);
    {
        g_autoptr (GMutexLocker) guard = g_mutex_locker_new (&global_poppler_lock);
        poppler_page_render (self->page, ctx);
    }
    cairo_destroy (ctx);

    return thumbnail;
}


static void
page_thumbnailer_thread (GTask        *task,
                         gpointer      source_object,
                         gpointer      task_data,
                         GCancellable *cancellable)
{
    SnicerPage *self = SNICER_PAGE (source_object);

    if (g_cancellable_is_cancelled (cancellable))
        return;

    g_task_return_pointer (task, snicer_page_render_thumbnail (self),
                           (GDestroyNotify) g_boxed_free);
}

static void
on_thumbnail_rendered (GObject      *source,
                       GAsyncResult *res,
                       gpointer      user_data)
{
    if (g_task_is_valid (G_TASK (res), source)) {
        SnicerPage *self = SNICER_PAGE (source);

        g_clear_pointer (&self->thumbnail, cairo_surface_destroy);
        self->thumbnail = g_task_propagate_pointer (G_TASK (res), NULL);
        g_object_notify_by_pspec (source, properties[PROP_THUMBNAIL]);
    }
}

/**
 * snicer_page_get_thumbnail:
 * @self: A #SnicerPage
 *
 * Will get the page's thumbnail representation. If the thumbnail is not yet
 * rendered, it will return a default icon and emit a notify for the icon
 * property once the background renderer is done.
 *
 * Returns: (transfer full): The #cairo_surface_t for the current thumbnail.
 * Use cairo_surface_destroy() to free.
 **/
cairo_surface_t *
snicer_page_get_thumbnail (SnicerPage *self)
{
    g_return_val_if_fail (self, NULL);

    // Take page mutex so the thread will not overwrite
    g_autoptr (GMutexLocker) guard = g_mutex_locker_new (&self->page_lock);

    if (self->thumbnail == NULL) {
        GError *error = NULL;
        GTask *task = g_task_new (self, self->cancellable, on_thumbnail_rendered,
                                  NULL);
        g_task_set_return_on_cancel (task, g_thread_use_default_impl);
        g_task_run_in_thread (task, page_thumbnailer_thread);

        self->thumbnail = gtk_icon_theme_load_surface (gtk_icon_theme_get_default (),
                                                       "x-office-document-symbolic",
                                                       256, 1.0, NULL, 0, &error);
    }

    return cairo_surface_reference (self->thumbnail);
}

const char *
snicer_page_get_title (SnicerPage *self)
{
    if (self->title == NULL)
        self->title = g_strdup_printf(_("Page %u"),
                                      podofo_page_get_page_number (PODOFO_PAGE (self)));

    return self->title;
}

static gboolean
snicer_page_rotate_internal (PodofoPage                   *page,
                             PodofoPageRotationDirection   direction,
                             GError                      **error)
{
    g_return_val_if_fail (SNICER_IS_PAGE (page), FALSE);

    SnicerPage *self = SNICER_PAGE (page);

    if (!(PODOFO_PAGE_CLASS (snicer_page_parent_class)->rotate (page, direction, error)))
        return FALSE;

    int w = cairo_image_surface_get_width (self->thumbnail);
    int h = cairo_image_surface_get_height (self->thumbnail);

    cairo_surface_t *dest = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, h, w);
    cairo_t *ctx = cairo_create (dest);
    cairo_translate (ctx, h * 0.5, w * 0.5);
    cairo_rotate (ctx, (double) direction * G_PI / 2.0);
    cairo_translate (ctx, -w * 0.5, -h * 0.5);
    cairo_set_source_surface (ctx, self->thumbnail, 0.0, 0.0);
    cairo_set_operator (ctx, CAIRO_OPERATOR_SOURCE);
    cairo_paint (ctx);
    cairo_destroy (ctx);

    g_clear_pointer (&self->thumbnail, cairo_surface_destroy);
    self->thumbnail = dest;

    g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_THUMBNAIL]);

    return TRUE;
}


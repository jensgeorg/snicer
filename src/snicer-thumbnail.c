#include "snicer-thumbnail.h"

#include <cairo.h>
#include <cairo-gobject.h>

struct _SnicerThumbnail
{
  GtkBox parent_instance;

  GtkWidget *thumbnail;
  GtkWidget *title;
  SnicerPage *page;
};

G_DEFINE_TYPE (SnicerThumbnail, snicer_thumbnail, GTK_TYPE_BOX)

enum {
  PROP_0,
  PROP_PAGE,
  PROP_PAGE_NR,
  PROP_THUMBNAIL,
  PROP_TITLE,
  PROP_SIZE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

SnicerThumbnail *
snicer_thumbnail_new (SnicerPage *page)
{
  return g_object_new (SNICER_TYPE_THUMBNAIL, "page-ptr", page, NULL);
}

static void
snicer_thumbnail_finalize (GObject *object)
{
  //SnicerThumbnail *self = (SnicerThumbnail *)object;

  G_OBJECT_CLASS (snicer_thumbnail_parent_class)->finalize (object);
}

static void
snicer_thumbnail_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
    SnicerThumbnail *self = SNICER_THUMBNAIL (object);

    switch (prop_id) {
    case PROP_PAGE:
        g_value_set_object (value, self->page);
        break;
    case PROP_PAGE_NR:
        g_value_set_uint (value,
                          podofo_page_get_page_number (PODOFO_PAGE (self->page)));
        break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
snicer_thumbnail_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  SnicerThumbnail *self = SNICER_THUMBNAIL (object);

  switch (prop_id)
    {
      case PROP_THUMBNAIL:
      if (g_value_get_boxed (value) != NULL) {
        gtk_image_set_from_surface (GTK_IMAGE (self->thumbnail), g_value_get_boxed (value));
      } else {
        gtk_image_set_from_icon_name (GTK_IMAGE (self->thumbnail), "x-office-document-symbolic", 0);
        gtk_image_set_pixel_size (GTK_IMAGE (self->thumbnail), 256);
      }
      break;
    case PROP_TITLE:
      gtk_label_set_text (GTK_LABEL (self->title), g_value_get_string (value));
      break;
    case PROP_SIZE:
      if (gtk_image_get_storage_type (GTK_IMAGE (self->thumbnail)) == GTK_IMAGE_ICON_NAME) {
        gtk_image_set_pixel_size (GTK_IMAGE (self->thumbnail), g_value_get_uint (value));
      }
      break;
    case PROP_PAGE:
      self->page = g_value_get_object (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
snicer_thumbnail_constructed (GObject *instance)
{
    SnicerThumbnail *self = SNICER_THUMBNAIL (instance);

    g_object_bind_property (self->page, "thumbnail",
                            self->thumbnail, "surface",
                            G_BINDING_DEFAULT | G_BINDING_SYNC_CREATE);

    g_object_bind_property (self->page, "title",
                            self, "title",
                            G_BINDING_DEFAULT | G_BINDING_SYNC_CREATE);
}

static void
snicer_thumbnail_class_init (SnicerThumbnailClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    object_class->finalize = snicer_thumbnail_finalize;
    object_class->get_property = snicer_thumbnail_get_property;
    object_class->set_property = snicer_thumbnail_set_property;
    object_class->constructed = snicer_thumbnail_constructed;

    gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Snicer/snicer-thumbnail.ui");
    gtk_widget_class_bind_template_child (widget_class, SnicerThumbnail, thumbnail);
    gtk_widget_class_bind_template_child (widget_class, SnicerThumbnail, title);
    properties[PROP_THUMBNAIL] = g_param_spec_boxed ("thumbnail", "thumbnail", "thumbnail",
                                                     CAIRO_GOBJECT_TYPE_SURFACE,
                                                     (G_PARAM_READWRITE
                                                      | G_PARAM_CONSTRUCT
                                                      | G_PARAM_STATIC_STRINGS));

    properties[PROP_TITLE] = g_param_spec_string ("title", "title", "title", NULL, G_PARAM_CONSTRUCT | G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

    properties[PROP_SIZE] = g_param_spec_uint ("size", "size", "size", 0, G_MAXUINT, 0, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

    properties[PROP_PAGE] = g_param_spec_object ("page-ptr", "page-ptr", "page-ptr",
                                                 SNICER_TYPE_PAGE,
                                                 G_PARAM_READWRITE |
                                                 G_PARAM_CONSTRUCT_ONLY|
                                                 G_PARAM_STATIC_STRINGS);

    properties[PROP_PAGE_NR] = g_param_spec_uint ("page-nr", "page-nr", "page-nr",
                                                  0, G_MAXUINT, 0,
                                                  G_PARAM_READABLE |
                                                  G_PARAM_STATIC_STRINGS);

    g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
snicer_thumbnail_init (SnicerThumbnail *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
} 

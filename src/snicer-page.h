#pragma once

#include "podofo-page.h"

#include <glib.h>
#include <glib-object.h>

#include <cairo.h>


G_BEGIN_DECLS

#define SNICER_TYPE_PAGE (snicer_page_get_type())

G_DECLARE_FINAL_TYPE (SnicerPage, snicer_page, SNICER, PAGE, PodofoPage)

cairo_surface_t *
snicer_page_get_thumbnail (SnicerPage *self);

const char *
snicer_page_get_title (SnicerPage *self);

guint
snicer_page_get_number (SnicerPage *self);


G_END_DECLS

#include "podofo-document.h"
#include "podofo-error.h"

#include <podofo/podofo.h>
#include <poppler.h>

namespace pdf = PoDoFo;

GQuark podofo_error_quark (void)
{
    return g_quark_from_static_string ("podofo-document-error-quark");
}

static PodofoPage *
podofo_document_internal_get_page (PodofoDocument *self, guint page, gpointer page_ptr);

static gboolean
podofo_document_internal_delete_page (PodofoDocument *self,
                                      guint           page,
                                      GError         **error);
struct _PodofoDocumentPrivate {
    pdf::PdfMemDocument *doc;
    GFile *file;
};
using PodofoDocumentPrivate = struct _PodofoDocumentPrivate;


G_DEFINE_TYPE_WITH_PRIVATE (PodofoDocument, podofo_document, G_TYPE_OBJECT)

namespace {
static PodofoDocumentPrivate *
get_instance_private(PodofoDocument *self)
{
    return reinterpret_cast<PodofoDocumentPrivate *> (podofo_document_get_instance_private (self));
}
};

enum {
  PROP_0,
  N_PROPS
};

//static GParamSpec *properties [N_PROPS];

PodofoDocument *
podofo_document_new (void)
{
  return PODOFO_DOCUMENT (g_object_new (PODOFO_TYPE_DOCUMENT, NULL));
}

static void
podofo_document_finalize (GObject *object)
{
  PodofoDocument *self = (PodofoDocument *)object;

  auto priv = get_instance_private(self);

  g_clear_object (&priv->file);
  delete priv->doc;
  priv->doc = nullptr;

  G_OBJECT_CLASS (podofo_document_parent_class)->finalize (object);
}

static void
podofo_document_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  //PodofoDocument *self = PODOFO_DOCUMENT (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
podofo_document_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  //PodofoDocument *self = PODOFO_DOCUMENT (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
podofo_document_class_init (PodofoDocumentClass *klass)
{
  klass->get_page = podofo_document_internal_get_page;
  klass->delete_page = podofo_document_internal_delete_page;

  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = podofo_document_finalize;
  object_class->get_property = podofo_document_get_property;
  object_class->set_property = podofo_document_set_property;
}

static void
podofo_document_init (PodofoDocument *self)
{
}

gboolean
podofo_document_open (PodofoDocument *self, GFile *file, GError **error)
{
    g_return_val_if_fail (PODOFO_IS_DOCUMENT (self), FALSE);

    podofo_document_close (self);

    try {
        auto priv = get_instance_private (self);
        g_autofree char *path = g_file_get_path (file);
        priv->doc = new pdf::PdfMemDocument ();
        priv->doc->Load(path);
        priv->file = G_FILE (g_object_ref (file));

        return TRUE;
    } catch (pdf::PdfError &err) {
        error << err;
    }

    return FALSE;
}

gboolean
podofo_document_write (PodofoDocument *self, GFile *file, GError **error)
{
    g_return_val_if_fail (PODOFO_IS_DOCUMENT (self), FALSE);
    auto priv = get_instance_private (self);

    auto overwrite = (file == nullptr) || g_file_equal (file, priv->file);

    if (overwrite) {
        GFileIOStream *stream = nullptr;
        g_autoptr(GFile) tmpfile = g_file_new_tmp (nullptr, &stream, error);
        if (tmpfile == nullptr)
            return FALSE;

        if (not g_io_stream_close (G_IO_STREAM (stream), nullptr, error))
            return FALSE;

        g_autofree char *path = g_file_get_path (tmpfile);
        g_autoptr(GFile) old_file = G_FILE (g_object_ref (priv->file));
        try {
            priv->doc->Save (path);
            podofo_document_close (self);
            if (not g_file_move (tmpfile, old_file,
                                 G_FILE_COPY_OVERWRITE,
                                 nullptr, nullptr, nullptr, error)) {
                return FALSE;
            }

            return podofo_document_open (self, old_file, error);
        } catch (pdf::PdfError &err) {
            error << err;

            return FALSE;
        }
    } else {
        g_autofree char *path = g_file_get_path (file);
        try {
            priv->doc->Save (path);

            podofo_document_close (self);
            return podofo_document_open (self, file, error);
        } catch (pdf::PdfError &err) {
            error << err;
        }

        return FALSE;
    }
}

void
podofo_document_close (PodofoDocument *self)
{
    g_return_if_fail (PODOFO_IS_DOCUMENT (self));
    auto priv = get_instance_private (self);

    delete priv->doc;
    priv->doc = nullptr;

    g_clear_object (&priv->file);
}

gboolean
podofo_document_rotate_page (PodofoDocument *self,
                             guint           page_nr,
                             gint            direction,
                             GError        **error)
{
    auto priv = get_instance_private (self);

    try {
        auto& page = priv->doc->GetPages().GetPageAt (page_nr);
        auto rotation_deg = (page.GetRotationRaw() + (direction * 90) + 360)  % 360;
        page.SetRotationRaw (rotation_deg);

        return TRUE;
    } catch (pdf::PdfError &err) {
        error << err;

        return FALSE;
    }
}

guint
podofo_document_get_n_pages (PodofoDocument *self)
{
    return static_cast<guint>(get_instance_private(self)->doc->GetPages().GetCount());
}

static gboolean
podofo_document_internal_delete_page (PodofoDocument *self,
                                      guint           page,
                                      GError         **error)
{
    auto priv = reinterpret_cast<PodofoDocumentPrivate *>(podofo_document_get_instance_private (self));

    try {
        priv->doc->GetPages().RemovePageAt(page);

        return TRUE;
    } catch (pdf::PdfError &err) {
        error << err;

        return FALSE;
    }
}

gboolean
podofo_document_delete_page (PodofoDocument *self,
                             guint           page,
                             GError        **error)
{
    PodofoDocumentClass *klass = PODOFO_DOCUMENT_GET_CLASS(self);

    g_return_val_if_fail(klass->delete_page != nullptr, FALSE);

    return klass->delete_page (self, page, error);
}

static PodofoPage *
podofo_document_internal_get_page (PodofoDocument *self, guint page_nr, gpointer page_ptr)
{
    return PODOFO_PAGE (g_object_new (PODOFO_TYPE_PAGE,
                                      "page", page_ptr,
                                      nullptr));
}

PodofoPage *
podofo_document_get_page (PodofoDocument *self, guint page)
{
    PodofoDocumentClass *klass = PODOFO_DOCUMENT_GET_CLASS(self);

    g_return_val_if_fail(klass->get_page != nullptr, nullptr);

    auto priv = reinterpret_cast<PodofoDocumentPrivate *>(podofo_document_get_instance_private (self));

    return klass->get_page (self, page, &priv->doc->GetPages().GetPageAt(page));
}

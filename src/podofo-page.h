#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define PODOFO_TYPE_PAGE (podofo_page_get_type())

G_DECLARE_DERIVABLE_TYPE (PodofoPage, podofo_page, PODOFO, PAGE, GObject)

typedef enum _PodofoPageRotationDirection {
  DIRECTION_CCW = -1,
  DIRECTION_CW  = 1
} PodofoPageRotationDirection;

struct _PodofoPageClass {
  GObjectClass parent_class;

  gboolean (*rotate)(PodofoPage                   *self,
                     PodofoPageRotationDirection   direction,
                     GError                      **error);

  gpointer padding[12];
};


gboolean
podofo_page_rotate (PodofoPage                   *self,
                    PodofoPageRotationDirection   direction,
                    GError                      **error);

guint
podofo_page_get_page_number (PodofoPage *self);

G_END_DECLS

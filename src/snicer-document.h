#pragma once

#include "podofo-document.h"

#include <glib-object.h>
#include <gio/gio.h>
#include <cairo.h>

G_BEGIN_DECLS

#define SNICER_TYPE_DOCUMENT (snicer_document_get_type())

#define SNICER_DOCUMENT_ERROR (snicer_document_error_quark())

G_DECLARE_FINAL_TYPE (SnicerDocument, snicer_document, SNICER, DOCUMENT, PodofoDocument)

SnicerDocument *
snicer_document_new (void);
gboolean
snicer_document_open (SnicerDocument *self,
                      GFile *file,
                      GCancellable *cancellable,
                      GError **error);
gboolean
snicer_document_close (SnicerDocument *self,
                       GError **error);

gboolean
snicer_document_write (SnicerDocument *self,
                       GFile *file,
                       GError **error);

guint
snicer_document_get_n_pages (SnicerDocument *self);

gboolean
snicer_document_is_modified (SnicerDocument *self);

gchar *
snicer_document_get_page_title (SnicerDocument *self,
                                guint           page);

const gchar *
snicer_document_get_file_name (SnicerDocument *self);

gboolean
snicer_document_rotate_pages (SnicerDocument *self,
                              guint *pages,
                              gsize pages_length,
                              gint direction,
                              GError **error);
gboolean
snicer_document_remove_pages (SnicerDocument *self,
                              guint *pages,
                              gsize pages_length,
                              GError **error);
gboolean
snicer_document_move_pages (SnicerDocument *self,
                            guint *pages,
                            gsize pages_length,
                            guint before_page,
                            GError **error);

GQuark
snicer_document_error_quark (void);

G_END_DECLS

#pragma once

#include <gio/gio.h>

#include <podofo/podofo.h>

GQuark
podofo_error_quark (void);
#define PODOFO_ERROR (podofo_error_quark())

inline GError **
operator<<(GError **error, PoDoFo::PdfError& pdf_error)
{
    g_set_error_literal (error, PODOFO_ERROR,
            static_cast<int>(pdf_error.GetCode()), pdf_error.what ());

    return error;
}
